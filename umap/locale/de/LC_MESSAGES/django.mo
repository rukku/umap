��    !      $  /   ,      �     �  %   �  0     ,   F     s     �      �     �     �     �            	   '     1     8     @  =   ^     �     �     �     �     �     �     �  %   �          *     8  �   @     +     <  w   I  �  �     Z  .   `  2   �  J   �  '   	     5	     T	     t	     �	     �	     �	  1   �	     �	  	   
  	   
  "   
  k   >
     �
     �
     �
     �
     �
     �
       -        K     Y  	   s    }     �     �  �   �                             	                          !                                
                                                                        About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in This is a demo instance, used for tests and pre-rolling releases. If you need a stable instance, please use <a href="%(stable_url)s">%(stable_url)s</a>. You can also host your own instance, it's <a href="%(repo_url)s">open source</a>! What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-06 16:09+0000
Last-Translator: Klumbumbus <simson.gertrud@gmail.com>
Language-Team: German (http://www.transifex.com/projects/p/umap/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Über Füge POIs hinzu: Marker, Linien, Flächen,... Und es ist <a href="%(repo_url)s">open source</a>! Stapelverarbeitung beim Importieren von geotechnischen Daten (GEOJson,...) Schaue dir %(current_user)s's Karten an Wähle die Ebenen deiner Karte Wähle die Lizenz deiner Daten. Zusammengestellt von Erstelle eine Karte Teile und binde deine Karte ein Feedback Lass dich inspirieren, schau dir diese Karten an. Neueste Karten Einloggen Ausloggen Verwalte Farben und Icons der POIs Verwalte Karteneinstellungen: eine Übersichtskarte anzeigen, den Nutzer beim Seitenaufruf lokalisieren,... Karteneinstellungen Mehr Meine Karten Keine Karte gefunden. Spiele mit der Demo Bitte wähle einen Anbieter Suchen Suche nach Karten, welche «%(q)s» enthalten Karten suchen Schau dir diese Karte an! Einloggen Dies ist eine Demo-Instanz und wird benutzt für Tests und Vorveröffentlichungen. Wenn du eine stabile Instanz benötigst, benutze bitte <a href="%(stable_url)s">%(stable_url)s</a>. Du kannst auch deine eigene Instanz hosten, es ist <a href="%(repo_url)s">open source</a>! Was kannst du tun? Was ist uMap Mit uMap kannst du in einer Minute Karten mit <a href="%(osm_url)s" />OpenStreetMap</a>-Ebenen erstellen und sie in deine eigene Internetseite einbinden. 