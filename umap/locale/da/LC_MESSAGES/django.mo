��    !      $  /   ,      �     �  %   �  0     ,   F     s     �      �     �     �     �            	   '     1     8     @  =   ^     �     �     �     �     �     �     �  %   �          *     8  �   @     +     <  w   I  �  �     _  -   b  1   �  1   �     �     	     +	     I	     R	     ^	     x	     �	     �	     �	     �	      �	  Y   �	     4
     F
  	   K
     U
     h
     w
     �
  (   �
     �
     �
     �
  �   �
     �     �  �   �                             	                          !                                
                                                                        About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in This is a demo instance, used for tests and pre-rolling releases. If you need a stable instance, please use <a href="%(stable_url)s">%(stable_url)s</a>. You can also host your own instance, it's <a href="%(repo_url)s">open source</a>! What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-08 12:25+0000
Last-Translator: Neogeografen <soren.johannessen@gmail.com>
Language-Team: Danish (http://www.transifex.com/projects/p/umap/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Tilføj POIs: markører, linjer, polygoner... Og det er <a href="%(repo_url)s">open source</a>! Batch import af struktureret geodata (GeoJSON...) Browse  %(current_user)s's kort Vælg lag for dit kort Vælg en licens for dine data Lavet af Lav et kort Indlejre og dele dit kort Feedback Få inspiration, browse i kort Sidste kort Login Log ud Håndterer POIs farver og ikoner Håndterer kortindstillinger: vis et miniaturekort, lokaliser brugeren ved indlæsning... Kortindstillinger Mere Mine kort Ingen kort fundet. Leg med demoen Vælg en udbyder Søg Søg efter kort der indeholder «%(q)s» Søg i kortene Se dette kort! Opret konto Dette er en demo som bruges til test og forhåndstesting. Hvis du har brug for en stabil testdemo, så brug <a href="%(stable_url)s">%(stable_url)s</a>.  Du kan webhoste din egen testdemo. det er <a href="%(repo_url)s">open source</a>! Hvad kan du? Hvad er uMap Med uMap kan du lave kort med <a href="%(osm_url)s" />OpenStreetMap</a> lag og på et minuts arbejde kan du indlejre disse på dit eget websted. 