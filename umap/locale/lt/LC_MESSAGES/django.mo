��    !      $  /   ,      �     �  %   �  0     ,   F     s     �      �     �     �     �            	   '     1     8     @  =   ^     �     �     �     �     �     �     �  %   �          *     8  �   @     +     <  w   I  �  �     �  +   �  1   �  .   �  +   %	  !   Q	     s	     �	     �	  %   �	     �	      �	      
     
  
   "
      -
  [   N
     �
     �
     �
     �
     �
     �
     
  "        6     L     i    z     �     �  �   �                             	                          !                                
                                                                        About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in This is a demo instance, used for tests and pre-rolling releases. If you need a stable instance, please use <a href="%(stable_url)s">%(stable_url)s</a>. You can also host your own instance, it's <a href="%(repo_url)s">open source</a>! What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2014-01-22 20:27+0000
Last-Translator: ramunasd <ieskok@ramuno.lt>
Language-Team: Lithuanian (http://www.transifex.com/projects/p/umap/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
 Apie Pridėti POI: žymės, linijos, poligonai.. Juk tai <a href="%(repo_url)s">atviras kodas</a>! Masiškai importuoti geo duomenis (GEOJson...) Peržiūrėti %(current_user)s žemėlapius Pasirinkti žemėlapio sluoksnius Nustatyti duomenų licenziją Pagamino Kurti žemėlapį Įterpti ir dalintis savo žemėlapiu Atsiliepimai Raskite įkvėpimą benaršydami Naujausi žemėlapiai Prisijungti Atsijungti Valdyti POI spalvas ir ikonėles Valdyti žemėlapio nustatymus: rodyti mini žemėlapį, automatiškai nustatyti padėtį.. Žemėlapio nustatymai Daugiau Mano žemėlapiai Nerasta. Išbandyti demo Pasirinkite teikėją Ieškoti Rasti žemėlapių pagal «%(q)s» Ieškoti žemėlapių Žiūrėti šį žemėlapį! Užsiregistruoti Tai demonstracinė versija, naudojama testavimui ir naujų versijų demonstravimui. Jei Jums reikia stabilios versijos, tada geriau naudokitės <a href="%(stable_url)s">%(stable_url)s</a>. Jūs taip pat gali pasileisti savo puslapį, juk tai <a href="%(repo_url)s">atviras kodas</a>! Ką galima padaryti? Kas yra uMap uMap leidžia susikurti savo žemėlapį naudojant <a href="%(osm_url)s" />OpenStreetMap</a> sluoksnius vos per minutę ir įterpti jį į savo puslapį. 