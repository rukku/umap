��    !      $  /   ,      �     �  %   �  0     ,   F     s     �      �     �     �     �            	   '     1     8     @  =   ^     �     �     �     �     �     �     �  %   �          *     8  �   @     +     <  w   I  �  �  	   Y  0   c  0   �  8   �  (   �     '	  "   B	     e	     r	      �	     �	     �	     �	  	   �	     �	  !   �	  V   
     h
     t
  
   y
     �
     �
     �
     �
  '   �
     �
          "  4  3     h       �   �                             	                          !                                
                                                                        About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in This is a demo instance, used for tests and pre-rolling releases. If you need a stable instance, please use <a href="%(stable_url)s">%(stable_url)s</a>. You can also host your own instance, it's <a href="%(repo_url)s">open source</a>! What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-05 21:58+0000
Last-Translator: yohanboniface <yohanboniface@free.fr>
Language-Team: French (http://www.transifex.com/projects/p/umap/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Ajouter des POI: marqueurs, lignes, polygones... Et c'est <a href="%(repo_url)s">open source</a>! Import des données géographiques en masse (GeoJSON...) Consulter les cartes de %(current_user)s Choisir les fonds de carte Choisir la licence de vos données Maintenu par Créer une carte Exporter et partager votre carte Feedback Naviguer dans les cartes Dernières cartes Connexion Déconnexion Choisir la couleur et les icônes Gérer les options de la carte: afficher une minicarte, géolocaliser l'utilisateur... Paramètres Plus Mes cartes Aucune carte trouvée. Tester la démo Merci de choisir un fournisseur Chercher Chercher des cartes contenant «%(q)s» Chercher des cartes Voir cette carte! Créer un compte Il s'agit d'un exemple de démonstration, utilisé pour les tests et validation avant diffusion. Si vous avez besoin d'une instance stable, s'il vous plaît utiliser <a href="%(stable_url)s">%(stable_url)s</a>. Vous pouvez aussi organiser votre propre exemple, il est  <a href="%(repo_url)s">open source</a>! Que pouvez-vous faire? uMap, c'est quoi uMap permet de créer des cartes personnalisées sur des fonds <a href="%(osm_url)s" />OpenStreetMap</a> en un instant et les afficher dans votre site. 