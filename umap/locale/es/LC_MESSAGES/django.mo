��             +         �     �  %   �  0   �  ,   &     S     r      �     �     �     �     �     �  	                     =   >     |     �     �     �     �     �     �  %   �     �     
                1  w   >  �  �     P  /   ]  5   �  4   �     �          -     L     Y     f     �     �     �     �     �  +   �  Y   �     M	     ^	  	   c	     m	     �	     �	     �	  #   �	     �	     �	     �	     	
     
  y   +
                      
                             	                                                                                             About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-05 21:58+0000
Last-Translator: yohanboniface <yohanboniface@free.fr>
Language-Team: Spanish (http://www.transifex.com/projects/p/umap/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Acerca de... Añade PDIs: Marcadores, líneas, polígonos... Y es de !<a href="%(repo_url)s">fuentes abiertas</a>! Importa en lotes datos geoestructurados (GEOJson...) Mapas de %(current_user)s Elige las capas de tu mapa Elige la licencia de tus datos Iniciado por Crea un mapa Incrusta y comparte tu mapa Contacto Inspírate, mira otros mapas Mapas recientes Ingresar Salir Elige los colores y los íconos de tus PDIs Administra las opciones de tu mapa: muestra un minimapa, localiza al usuario al cargar... Opciones de mapa Más Mis Mapas No se encontraron mapas. Juega con el demo Por favor elija un proveedor Buscar Busca mapas que contengan «%(q)s» Buscar Mapas !Ver este mapa! Registrarse ¿Qué puedes hacer? Qué es uMap uMap te permite crear mapas con capas de <a href="%(osm_url)s" />OpenStreetMap</a> en un minuto y embeberlas en tu sitio. 