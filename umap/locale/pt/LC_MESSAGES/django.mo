��            )   �      �     �  %   �  ,   �     �           7     X     e     r     �     �  	   �     �     �     �  =   �     #     0     5     =     L     _     x  %        �     �     �     �     �  �  �     �  0   �  0   �  %   �       (   *     S     _     m  	   �     �     �     �     �  "   �  E   �     <     H  
   M     X     p     ~     �  %   �     �     �     �     �     	                  	                                                                                
                                       About Add POIs: markers, lines, polygons... Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in What can you do? What is uMap Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-05 21:58+0000
Last-Translator: yohanboniface <yohanboniface@free.fr>
Language-Team: Portuguese (http://www.transifex.com/projects/p/umap/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 Acerca Adicionar objetos: marcos, linhas, polígonos... Carregar ficheiros georeferenciados (GEOJson...) Consulta os mapas de %(current_user)s Escolher o mapa de fundo Escolher uma licença para os seus dados Mantido por Criar um mapa Exportar e partilhar seu mapa Contactar Navega nos mapas existente Últimos mapas Identificar-se Sair Modificar cor e ícone dos objetos Gerir diversas opções: mostrar um minimapa, localizar o usuário... Parâmetros Mais Meus mapas Nenhum mapa encontrado. Testar a demo Por favor escolha um provedor Procurar Procurar mapas que incluam «%(q)s»  Procurar mapas Ver este mapa! Criar conta O que podes fazer O que é uMap 