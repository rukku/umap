��    !      $  /   ,      �     �  %   �  0     ,   F     s     �      �     �     �     �            	   '     1     8     @  =   ^     �     �     �     �     �     �     �  %   �          *     8  �   @     +     <  w   I  �  �     [  D   c  6   �  2   �  !   	  8   4	     m	     �	     �	  7   �	     �	     �	     
     "
     5
  ?   N
  X   �
     �
     �
             !   "  #   D     h  0   m     �     �     �    �  #   �     	  �                                	                          !                                
                                                                        About Add POIs: markers, lines, polygons... And it's <a href="%(repo_url)s">open source</a>! Batch import geostructured data (GEOJson...) Browse %(current_user)s's maps Choose the layers of your map Choose the license for your data Cooked up by Create a map Embed and share your map Feedback Get inspired, browse maps Last maps Log in Log out Manage POIs colours and icons Manage map options: display a minimap, locate user on load… Map settings More My maps Not map found. Play with the demo Please choose a provider Search Search for maps containing «%(q)s»  Search maps See this map! Sign in This is a demo instance, used for tests and pre-rolling releases. If you need a stable instance, please use <a href="%(stable_url)s">%(stable_url)s</a>. You can also host your own instance, it's <a href="%(repo_url)s">open source</a>! What can you do? What is uMap uMap let you create maps with <a href="%(osm_url)s" />OpenStreetMap</a> layers in a minute and embed them in your site. Project-Id-Version: uMap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-05 22:57+0100
PO-Revision-Date: 2013-12-05 21:58+0000
Last-Translator: yohanboniface <yohanboniface@free.fr>
Language-Team: Finnish (http://www.transifex.com/projects/p/umap/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Tietoja Lisää tarvittavat POIt: karttamerkkejä, viivoja, monikulmioita... <a href="%(repo_url)s">Avoin lähdekoodi</a> rulettaa! Tuo joukko strukturoitua paikkatietoa (GEOJson...) Selaa %(current_user)s:n karttoja Valitse karttaasi sopivat taustakartat ja data-kerrokset Valitse tiedoillesi lisenssi Tämän sopan keitti Luo uusi kartta Jaa karttasi muille ja/tai liitä se muihin sivustoihin Palaute Inspiraatiota! Selaa karttoja Viimeisimmät kartat Kirjaudu palveluun Kirjaudu ulos palvelusta Valitse ja hallinnoi POI-merkintöjen värit ja karttakuvakkeet Hallitse kartta-optiot: näytä mini-kartta, paikanna käyttäjä sivun latauksessa, ... Kartan asetukset Lisää Omat kartat Karttaa ei löytynyt. Tongi demoa sielusi kyllyydestä! Valitse mieleisesi palveluntarjoaja Etsi Etsi hakusanan/-t «%(q)s» sisältävät kartat Etsi karttoja Katso tätä karttaa! Kirjaudu palveluun Tämä on uMapin demo-instanssi, jota käytetään testaamiseen ja väliversioille. Jos tarvitset vakaan version, niin käytäthän <a href="%(stable_url)s">%(stable_url)s</a> :a. Voit myös tarjota oman instanssin - <a href="%(repo_url)s">avoin lähdekoodi</a> rulettaa! Mitä kaikkea uMapillä voi tehdä? Mikä uMap on? uMap mahdollistaa <a href="%(osm_url)s" />OpenStreetMap</a>-pohjaisten räätälöityjen karttojen luomisen ja liittämisen verkkosivuusi muutamassa minuutissa. 